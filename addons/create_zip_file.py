from __future__ import print_function
import os
import sys
import re
import time
import zipfile

ADDON_VERSION = (1, 0, 0) #!! Warning is overwritten by the script `set_version.sh` which runs in CI

ADDON_VER_STR = "{}-{}-{}".format(*ADDON_VERSION)

GIT_HASH = 'dev' #!! Warning is overwritten by the script `set_version.sh` which runs in CI

def zip_addon(addon):
	bpy_module = re.sub(".py", "", os.path.basename(os.path.realpath(addon)))
	zfile = os.path.realpath("../{}_{}_{}.zip".format(bpy_module, ADDON_VER_STR, GIT_HASH.replace("#", "")))

	print("Zipping addon - {0}".format(bpy_module))

	zf = zipfile.ZipFile(zfile, "w")
	if os.path.isdir(addon):
		for dirname, subdirs, files in os.walk(addon):
			if dirname.endswith("__pycache__"):
				print("OMIT DIR:", dirname)
				continue

			print("ADD DIR:", dirname)

			zf.write(dirname)
			for filename in files:
				if filename.endswith(".pyc"):
					print("OMIT FILE:", filename)
					continue

				print("ADD FILE:", filename)
				zf.write(os.path.join(dirname, filename))
	else:
		zf.write(addon)
	zf.close()
	return (bpy_module, zfile)


zip_addon("chordata")