# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

if "bpy" in locals():
	import imp
	imp.reload(out)
	imp.reload(timer)

else:
	from . import out
	from . import timer

import bpy
from datetime import datetime

def get_OP_blender_type(name):
	chord_op_namespace = "chordata."
	if name.startswith(chord_op_namespace):
		name = name[len(chord_op_namespace):]

	return getattr(bpy.types,"CHORDATA_OT_" + name)


def time_str():
	return datetime.now().isoformat(sep=' ', timespec="seconds")


# def set_world_rotations_in_action(armature_ob, action):
# 	for bone in armature_ob.data.bones:
# 		q = bone.matrix_local.to_quaternion()
# 		if bone.name not in action.chordata_world_rotations: 
# 			item = action.chordata_world_rotations.add()
# 			item.name = bone.name 
# 		else:
# 			item = action.chordata_world_rotations[bone.name]

# 		item.value = bone.matrix_local.to_quaternion()

# 	duration = int(action.frame_range[1] - action.frame_range[0])
	
# 	# duration = min(duration, 100)

# 	if not armature_ob.animation_data:
# 		armature_ob.animation_data_create()

# 	armature_ob.animation_data.action = action
# 	starting_frame = bpy.context.scene.frame_current

# 	for count in range(duration):
# 		frame =  action.frame_range[0] + (count % duration)
# 		print("insert key @ frame {}".format(frame))
# 		bpy.context.scene.frame_current = frame
# 		for bone in armature_ob.pose.bones:
# 			bone.rotation_q_pose_space = bone.matrix.to_quaternion().copy()
# 			print(bone.rotation_q_pose_space)

# 			key_insert = armature_ob.keyframe_insert(\
# 					'pose.bones["{}"].rotation_q_pose_space'.format(bone.name),
# 						index=-1, 
# 						frame=frame, 
# 						# group="(pose space) " + bone.name)
# 						group=bone.name)


# #https://developer.blender.org/T51096
# # >>> ob.pose.bones['head'].chordata.path_from_id("rotation_q_pose_space")
# # Traceback (most recent call last):
# #   File "<blender_console>", line 1, in <module>
# # ValueError: Chordata_Bone_Properties.path_from_id("rotation_q_pose_space") found, but does not support path creation

	
# 	bpy.context.scene.frame_current = starting_frame		
