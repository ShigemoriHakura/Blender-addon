# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
from mathutils import Quaternion
from .basenode import ChordataBaseNode, set_dirty_flag
from ..ops import engine

class CustomSettings(bpy.types.PropertyGroup):
	"""Property group for the custom node settings"""
	dump_to_console: bpy.props.BoolProperty(
		name="Dump to system console", default=False, update=set_dirty_flag)

	rotate_current_ob: bpy.props.BoolProperty(
		name="Rotate selected object", default=False, update=set_dirty_flag)


# =========================================
# =           CUSTOM NODE CLASS           =
# =========================================

class CustomNode(ChordataBaseNode):
	'''Modify this class to create a node with custom behaviour'''
	bl_idname = 'CustomNodeType'
	bl_label = "Custom Node"

	# === Property Group Pointer ===================================================
	# the settings in this property group will be exposed to the Chordata engine 
	# through the property `id_settings`
	settings: bpy.props.PointerProperty(type=CustomSettings)
	# ==============================================================================

	def init(self, context):
		"""This function is executed on node creation"""
		self.width = 350.0
		self.inputs.new('DataStreamSocketType', "data_in")
		self.outputs.new('DataStreamSocketType', "data_out")

	def draw_buttons(self, context, layout):
		"""Here you can draw the GUI of the node"""
		layout.label(text="The behaviour of this node can be customized.")
		layout.operator("wm.url_open", text="More info here").url = "http://forum.chordata.cc"

		layout.prop(self.settings, "dump_to_console")
		layout.prop(self.settings, "rotate_current_ob")

	def draw_label(self):
		return "Custom Node"


	@engine.datatarget_handler(engine.DataTarget.Q)
	def out_q(self, packet):
		"""Each Chordata Node can handle one or more DataTargets coming from the capture stream.
		Node methods should be decorated with one or more engine.datatarget_handler to made them 
		available to the Chordata engine."""
		
		#Iterate over all the messages of the incoming packet
		for msg in packet.get():
			if self.id_settings.dump_to_console:
				self._output("Custom node received a Quaternion: ", msg)

			if self.id_settings.rotate_current_ob:
				ob = bpy.context.object
				if ob.rotation_mode == 'QUATERNION':
					ob.rotation_quaternion = msg.payload
				elif ob.rotation_mode == 'XYZ':
					ob.rotation_euler = Quaternion(msg.payload).to_euler()
				else:
					print("Object with unsoported rotation_mode")


	@engine.helper_method
	def _output(self, label, msg):
		"""If you need to create subroutines, to call from the main datatarget_handlers
		they should be decorated as engine.helper_method"""

		#Print the contents of the msg
		print(label, msg.payload, "with subtarget", msg.subtarget )


# ======  End of CUSTOM NODE CLASS  =======

# the following lines tell blender to apply the modifications on the custom node

to_register = (CustomSettings, CustomNode)

if __name__ == '__main__':
	from bpy.utils import register_class
	for cls in to_register:
		register_class(cls)