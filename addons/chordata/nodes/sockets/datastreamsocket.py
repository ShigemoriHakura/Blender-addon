# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from bpy.types import NodeSocket
from ... import defaults

# Custom socket type
class DataStreamSocket(NodeSocket):
    '''Socket for transferring Chordata stream data'''
    bl_idname = 'DataStreamSocketType'
    bl_label = "Data Stream Node Socket"

    def draw(self, context, layout, node, text):
        if self.is_output and self.node.bl_idname == "RecordNodeType":
            layout.label(text="Playback Data Out")
        elif self.is_output:
            layout.label(text="Data Out")
        elif not self.is_output:
            layout.label(text="Data In")
            
    # Socket color
    def draw_color(self, context, node):
        return defaults.color_red 

to_register = (DataStreamSocket,)