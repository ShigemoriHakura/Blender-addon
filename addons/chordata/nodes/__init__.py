# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

if "bpy" in locals():
    import imp
    imp.reload(basenode)
    imp.reload(sockets)
    imp.reload(armature)
    imp.reload(notochord)
    imp.reload(record)
    imp.reload(forward)
    imp.reload(dump)
    imp.reload(testcube)
    imp.reload(vector)
    imp.reload(stats)
    imp.reload(custom)

else:
    from . import basenode
    from . import sockets
    from . import armature
    from . import notochord
    from . import record
    from . import forward
    from . import dump
    from . import testcube
    from . import vector
    from . import stats
    from . import custom

import bpy
from nodeitems_utils import NodeCategory, NodeItem

class ChordNodeCategory(NodeCategory):
    @classmethod
    def poll(cls, context):
        return context.space_data.tree_type == 'ChordataTreeType'

node_categories = [
    ChordNodeCategory('CHORD_MAIN_NODES', "Main nodes", items=[
        NodeItem("ArmatureNodeType"),
        NodeItem("NotochordNodeType"),
        NodeItem("RecordNodeType"),
        NodeItem("ForwardNodeType"),
    ]),
    ChordNodeCategory('CHORD_TEST_NODES', "Probe Nodes", items=[
        NodeItem("DumpNodeType"),
        NodeItem("TestCubeNodeType"),
        NodeItem("VectorNodeType"),
        NodeItem("StatsNodeType"),
        NodeItem("CustomNodeType"),
        
    ])
]


to_register = basenode.to_register \
    + armature.to_register \
    + notochord.to_register \
    + record.to_register \
    + forward.to_register \
    + dump.to_register \
    + testcube.to_register \
    + vector.to_register \
    + stats.to_register \
    + custom.to_register



def register():
    from bpy.utils import register_class
    for cls in to_register:
        register_class(cls)

    sockets.register()


def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(to_register):
        unregister_class(cls)

    sockets.unregister()
