# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from bpy.types import Node, NodeSocket, PropertyGroup
from bpy.props import BoolProperty,PointerProperty
from ..chordatatree import ChordataTreeNode
from ..utils import out

def set_dirty_flag(self, context):
    self.id_data.dirty = True

def verify_links_valid(node):
    for inp in node.inputs:
        for link in inp.links:
            if link.from_socket.bl_idname == link.to_socket.bl_idname:
                continue
            else:
                out.debug("node link is NOT valid! Removing it..")
                node_tree = node.id_data
                node_tree.links.remove(link)

class ChordataBaseNode(Node, ChordataTreeNode): #pragma: no cover
    settings= None

    helper_funcs = tuple()

    def init_stats(self):
        self.outputs.new('StatsSocketType', "stats")

    def draw_buttons_ext(self, context, layout):
        if 'stats' in self.outputs:
            layout.label( text = "Stats configuration")
            stats_socket = self.outputs['stats']
            layout.prop(stats_socket.config, 'track_stats')
        else:
            pass

    def update(self):
        self.id_data.dirty = True
        verify_links_valid(self)
        
    def on_engine_node_init(self):
        pass

    def on_engine_node_stop(self):
        pass


to_register = tuple()