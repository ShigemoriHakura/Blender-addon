# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from bpy.types import Text as bpy_Text
import xml.etree.ElementTree as ET
import xml.dom.minidom
from .. import defaults

from ..defaults import configurations_dict


def generate_configurations_dict(notochord_settings):
    # MUST CAST TO STRING NON STRING VALUES
    # iterating through class custom properties is not possibile using items() method since it doesn't return properties that were not manually set to something
    # iteration in class __annotations__ might be implemented in the future
    # https://blenderartists.org/t/2-80-cheat-sheet-for-updating-add-ons/1148974
    if notochord_settings.transmission_mode == 'UNICAST' and notochord_settings.local_address:
        configurations_dict['Configuration']['Communication']['Ip']['text'] = notochord_settings.local_address
    elif notochord_settings.transmission_mode == 'BROADCAST' and notochord_settings.net_submask:
        configurations_dict['Configuration']['Communication']['Ip']['text'] = notochord_settings.net_submask
    elif notochord_settings.transmission_mode == 'MULTICAST' and notochord_settings.multi_id_group:
        configurations_dict['Configuration']['Communication']['Ip']['text'] = notochord_settings.multi_id_group
    else:
        raise ValueError("Detected empty network setting!")
    if notochord_settings.kc_revision and notochord_settings.log and notochord_settings.osc_base:
        configurations_dict['Configuration']['Communication']['Port']['text'] = str(
            notochord_settings.local_osc_port)
        if notochord_settings.transmission_mode == 'BROADCAST':
            configurations_dict['Configuration']['Communication']['Ip']['text'] = notochord_settings.net_submask
        elif notochord_settings.transmission_mode == 'MULTICAST':
            configurations_dict['Configuration']['Communication']['Ip']['text'] = notochord_settings.multi_id_group
        configurations_dict['Configuration']['KC_revision']['text'] = notochord_settings.kc_revision
        configurations_dict['Configuration']['Communication']['Log']['text'] = notochord_settings.log
        configurations_dict['Configuration']['Communication']['Send_rate']['text'] = str(
            notochord_settings.send_rate)
        configurations_dict['Configuration']['Communication']['Verbosity']['text'] = str(
            notochord_settings.verbosity)
        configurations_dict['Configuration']['Osc']['Base']['text'] = notochord_settings.osc_base
        return configurations_dict
    else:
        raise ValueError("Detected empty setting!")


def generate_xml(notochord_settings, text_datablock, pretty_print=True):
    header_comment = ET.Comment(text= defaults.XML_COMMENT)
    # root.append(header_comment)
    # generate root element for congif xml
    root = ET.Element("Chordata", {'version': defaults.ADDON_VER_STR})
    # generate the xml elements hierarchy and append them to the root

    config = generate_xml_element(generate_configurations_dict(
        notochord_settings)['Configuration'])
    config_raw_string = ET.tostring(config, encoding="unicode")
    dom = xml.dom.minidom.parseString(config_raw_string)
    config_pretty_string = dom.toprettyxml()
    armature = load_armature_xml(text_datablock)
    config_pretty_xml = ET.fromstring(config_pretty_string)
    root.append(config_pretty_xml)
    root.append(armature)
    final_xml = ET.tostring(root, encoding="unicode")
    # return final_xml[final_xml.find("<Chordata"):]
    return "{}\n{}".format(ET.tostring(header_comment, encoding="unicode"), final_xml)


def load_armature_xml(text_datablock):
    if type(text_datablock) == bpy_Text:
        xml_string = text_datablock.as_string()
    elif type(text_datablock) == str:
        xml_string = text_datablock
    else:
        raise TypeError(
            "The argument should be of type 'bpy_types.Text' or 'str'")

    if not xml_string.startswith("<Armature>"):
        raise ValueError(
            "The armature text doesn't start with an Armature element")

    try:
        # parse xml element from datablock
        armature_xml = ET.fromstring(xml_string)
        return armature_xml
    except ET.ParseError:
        raise ValueError("The armature text isn't a valid XML")


def generate_xml_element(data):
    # check for valid XML tag
    if 'tag' in data and data['tag']:
        # check for valid XML attributes
        if 'attributes' in data and data['attributes']:
            # create XML element with both tag and attributes
            el = ET.Element(data['tag'], data['attributes'])
        else:
            # create XML element only with tag
            el = ET.Element(data['tag'])
        # check for valid text
        if 'text' in data and data['text']:
            # set text
            el.text = data['text']
        # recursively find, creates and append child elements
        for k, v in data.items():
            if k != 'tag' and k != 'text' and k != 'attributes':
                el.append(generate_xml_element(data[k]))
        return el
    else:
        raise KeyError(
            'The dict passed to this function must have a "tag" key with a non-empty value.')
