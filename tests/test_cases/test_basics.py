import pytest
import addon_utils
import sys

def get_version(chordata_addon):
	mod = sys.modules[chordata_addon]
	return mod.bl_info.get("version", (-1, -1, -1))

def test_versionID_pass(chordata_addon):
	expect_version = (1, 0, 0)
	return_version = get_version(chordata_addon)
	assert expect_version == return_version


def test_versionID_fail(chordata_addon):
	expect_version = (0, 1, 1)
	return_version = get_version(chordata_addon)
	assert not expect_version == return_version

def test_blender_version(bpy_test):
	assert bpy_test.data.version >= (2, 80 ,0), "Blender version should be greater or equal to 2.80"


def test_new_node(chordata_nodetree, bpy_test):

	assert len(chordata_nodetree.nodes) == 0
	assert len(bpy_test.data.node_groups) == 1

def test_new_nodetree(chordata_nodetree, bpy_test):
	assert chordata_nodetree == bpy_test.data.node_groups['Chordata_test_nodetree']
	assert len(bpy_test.data.node_groups) == 1

def test_chordata_import(chordata_module):
	assert chordata_module.bl_info is not None
	assert chordata_module.ops is not None
	assert chordata_module.ops.xml_generator is not None

def test_logger(bpy_test, chordata_module, chordata_defaults, mocker):
	import logging
	logger = chordata_module.utils.out.get_logger()
	assert logger is not None
	assert len(logger.handlers) == 1
	logger.debug = mocker.Mock()
	logger.info = mocker.Mock()
	logger.warning = mocker.Mock()
	logger.error = mocker.Mock()

	args = ("one", "two")
	chordata_module.utils.out.debug(*args)
	chordata_module.utils.out.info(*args)	
	chordata_module.utils.out.warn(*args)	
	chordata_module.utils.out.warning(*args)
	chordata_module.utils.out.error(*args)	

	logger.debug.assert_called_once_with(*args)
	logger.info.assert_called_once_with(*args)
	logger.warning.assert_called_with(*args)
	assert logger.warning.call_count == 2
	logger.error.assert_called_once_with(*args)

	assert logger.level == logging.INFO
	bpy_test.context.window_manager.chordata_debug = True
	assert logger.level == logging.DEBUG
	bpy_test.context.window_manager.chordata_debug = False
	assert logger.level == logging.INFO
	
	chordata_module.utils.out.clean_logger()
	assert len(logger.handlers) == 0

