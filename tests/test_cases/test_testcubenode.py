import pytest

def test_init(chordata_nodetree):
    testcubenode = chordata_nodetree.nodes.new("TestCubeNodeType")
    assert testcubenode.inputs[0].bl_idname == 'DataStreamSocketType'
    assert testcubenode.outputs[0].bl_idname == 'DataStreamSocketType'

def test_target_object_changed(chordata_nodetree, basic_cube):
    testcubenode = chordata_nodetree.nodes.new("TestCubeNodeType")
    assert basic_cube.rotation_mode != 'QUATERNION'
    testcubenode.settings.target_object = basic_cube
    assert basic_cube.rotation_mode == 'QUATERNION'
    assert chordata_nodetree.dirty == True

def test_target_object_changed_to_None(chordata_nodetree, basic_cube):
    testcubenode = chordata_nodetree.nodes.new("TestCubeNodeType")
    testcubenode.settings.target_object = basic_cube
    chordata_nodetree.dirty = False
    testcubenode.settings.target_object = None
    assert chordata_nodetree.dirty == True    

@pytest.fixture
def tcube_engine_node(chordata_nodetree, engine):
    testcubenode = chordata_nodetree.nodes.new("TestCubeNodeType")
    return engine.EngineNode(testcubenode)


def test_subtarget_enum(tcube_engine_node, basic_cube, copp_Q_packet):
    packet_elements = copp_Q_packet._get_elements()
    tcube_engine_node(copp_Q_packet)
    assert packet_elements[0].subtarget in tcube_engine_node.id_tree.subtargets
    assert packet_elements[1].subtarget in tcube_engine_node.id_tree.subtargets
    assert packet_elements[2].subtarget in tcube_engine_node.id_tree.subtargets
    assert len(tcube_engine_node.id_tree.subtargets) == 3 

def test_object_mod(tcube_engine_node, basic_cube, copp_Q_packet, rotated_quats):
    from mathutils import Euler, Quaternion
    tcube_engine_node.id_settings.target_object = basic_cube
    identity_quat = Quaternion((1,0,0,0))
    assert basic_cube.rotation_quaternion == identity_quat
    
    # Set the subtarget manually
    packet_elements = copp_Q_packet._get_elements()
    tcube_engine_node.add_subtarget(packet_elements[1].subtarget)
    tcube_engine_node.id_settings.selected_subtarget = packet_elements[1].subtarget
    
    # The selected object should take the rotation value 
    # from the correct message in the packet
    ret_packet = tcube_engine_node(copp_Q_packet)
    assert tcube_engine_node.id_settings.selected_subtarget == packet_elements[1].subtarget
    assert basic_cube.rotation_quaternion == rotated_quats[1]
    
    # The returned packet should be equal to the original 
    assert ret_packet.target == copp_Q_packet.target
    assert ret_packet._get_elements() == packet_elements 