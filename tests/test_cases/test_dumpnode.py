import pytest


@pytest.fixture
def mock_console_write(mocker, gui):
    _tmp = gui.console_write
    gui.console_write = mocker.Mock()
    yield
    gui.console_write = _tmp


def test_dump_printQtosystem(chordata_nodetree, engine, capsys, copp_Q_packet):

    debug_node = chordata_nodetree.nodes.new('DumpNodeType')
    debug_node.settings.quaternion_log = True
    debug_node.settings.log_destination = 'SYSTEM'
    node_1 = engine.EngineNode(debug_node)

    payload = copp_Q_packet._get_elements()[0].payload
    node_1(copp_Q_packet)
    captured = capsys.readouterr()
    assert captured.out.find( node_1.name ) != -1
    assert captured.out.find( str(payload) ) != -1


def test_dump_printROTtosystem(chordata_nodetree, engine, capsys, copp_Q_packet):

    debug_node = chordata_nodetree.nodes.new('DumpNodeType')
    debug_node.settings.rotation_log = True
    debug_node.settings.log_destination = 'SYSTEM'
    node_1 = engine.EngineNode(debug_node)

    copp_Q_packet.target = engine.DataTarget.ROT
    node_1(copp_Q_packet)
    captured = capsys.readouterr()
    assert captured.out.find( node_1.name ) != -1


def test_dump_printQtoblender(chordata_nodetree, engine, capsys, gui, mock_console_write, copp_Q_packet):
    debug_node = chordata_nodetree.nodes.new('DumpNodeType')
    debug_node.settings.quaternion_log = True
    debug_node.settings.log_destination = 'BLENDER'
    node_1 = engine.EngineNode(debug_node)

    n_msgs = len(copp_Q_packet._get_elements()) 
    node_1(copp_Q_packet)
    captured = capsys.readouterr()
    assert captured.out == ''
    gui.console_write.assert_called()
    assert gui.console_write.call_count == n_msgs


def test_dump_printROTtoblender(chordata_nodetree, engine, capsys, gui, mock_console_write, copp_Q_packet):
    debug_node = chordata_nodetree.nodes.new('DumpNodeType')
    debug_node.settings.rotation_log = True
    debug_node.settings.log_destination = 'BLENDER'
    node_1 = engine.EngineNode(debug_node)

    n_msgs = len(copp_Q_packet._get_elements()) 
    copp_Q_packet.target = engine.DataTarget.ROT
    node_1(copp_Q_packet)
    captured = capsys.readouterr()
    assert captured.out == ''
    gui.console_write.assert_called()
    assert gui.console_write.call_count == n_msgs

def test_dump_noprintROT(chordata_nodetree, engine, capsys, gui, mock_console_write, copp_Q_packet):
    debug_node = chordata_nodetree.nodes.new('DumpNodeType')
    debug_node.settings.rotation_log = False
    node_1 = engine.EngineNode(debug_node)

    copp_Q_packet.target = engine.DataTarget.ROT
    node_1(copp_Q_packet)
    captured = capsys.readouterr()
    assert captured.out == ''
    debug_node.settings.log_destination = 'BLENDER'
    node_1(copp_Q_packet)
    gui.console_write.assert_not_called()


def test_dump_noprintQ(chordata_nodetree, engine, capsys, gui, mock_console_write, copp_Q_packet):
    debug_node = chordata_nodetree.nodes.new('DumpNodeType')
    debug_node.settings.quaternion_log = False
    node_1 = engine.EngineNode(debug_node)

    node_1(copp_Q_packet)
    captured = capsys.readouterr()
    assert captured.out == ''
    debug_node.settings.log_destination = 'BLENDER'
    node_1(copp_Q_packet)
    gui.console_write.assert_not_called()

def test_dump_print_else(chordata_nodetree, engine, capsys, gui, mock_console_write, copp_Q_packet):
    debug_node = chordata_nodetree.nodes.new('DumpNodeType')
    debug_node.settings.raw_log = True
    debug_node.settings.position_log = True
    debug_node.settings.comm_log = True
    debug_node.settings.err_log = True
    debug_node.settings.raw_log = True
    debug_node.settings.extra_log = True
    
    debug_node.settings.log_destination = 'BLENDER'
    node_1 = engine.EngineNode(debug_node)

    n_msgs = len(copp_Q_packet._get_elements()) 
    total_msgs = 0
    packet = copp_Q_packet
    packet.target = engine.DataTarget.RAW
    node_1(packet)
    packet.restore()
    total_msgs += n_msgs

    packet.target = engine.DataTarget.POS
    node_1(packet)
    packet.restore()
    total_msgs += n_msgs

    packet.target = engine.DataTarget.COMM
    node_1(packet)
    packet.restore()
    total_msgs += n_msgs

    packet.target = engine.DataTarget.ERR
    node_1(packet)
    packet.restore()
    total_msgs += n_msgs

    packet.target = engine.DataTarget.EXTRA
    node_1(packet)
    packet.restore()
    total_msgs += n_msgs

    gui.console_write.assert_called()
    assert gui.console_write.call_count == total_msgs


    debug_node.settings.raw_log = False
    debug_node.settings.position_log = False
    debug_node.settings.comm_log = False
    debug_node.settings.err_log = False
    debug_node.settings.raw_log = False
    debug_node.settings.extra_log = False

    packet.target = engine.DataTarget.RAW
    node_1(packet)
    packet.restore()

    packet.target = engine.DataTarget.POS
    node_1(packet)
    packet.restore()

    packet.target = engine.DataTarget.COMM
    node_1(packet)
    packet.restore()

    packet.target = engine.DataTarget.ERR
    node_1(packet)
    packet.restore()

    packet.target = engine.DataTarget.EXTRA
    node_1(packet)

    assert gui.console_write.call_count == total_msgs