import pytest

def test_datatarget_handler_decorator(base_fake_nodetree, engine):
    t_node, n_node, f_node = base_fake_nodetree
    assert engine.DataTarget.Q in f_node.some_handler._datatarget_handlers
    assert engine.DataTarget.ROT in f_node.some_handler._datatarget_handlers


def test_engine_parse(base_fake_nodetree, engine, copp_Q_packet):
    t_node, n_node, f_node = base_fake_nodetree

    engine_instance = engine.Engine()
    # parse_tree call with function fetching by name active
    engine_root = engine_instance.parse_tree(n_node, fetch_fns_by_name = True)

    assert len(engine_root.children) == 1 
    second_node = engine_root.children[0] 
    assert second_node.from_node == t_node.name
    assert len(second_node.children) == 1
    third_node = second_node.children[0]
    assert third_node.from_node == f_node.name

    assert hasattr(third_node, "fake_fn")
    args = ("First arg", "Second arg")
    res = third_node.fake_fn(*args)
    assert res == (third_node, ) + args
    assert third_node.exposed_handlers[engine.DataTarget.ROT]() == "ROT & Q handler response"
    assert third_node.exposed_handlers[engine.DataTarget.Q]() == "ROT & Q handler response"
    assert third_node.exposed_handlers[engine.DataTarget.RAW]() == "RAW handler response"

    assert second_node.engine == engine_instance
    assert third_node.engine == engine_instance

def test_engine_parse_wrong(chordata_nodetree, base_fake_nodetree, fake_regular_node, engine, copp_Q_packet):
    t_node, n_node, f_node = base_fake_nodetree
    engine_instance = engine.Engine()
    engine_root = engine_instance.parse_tree(n_node)
    third_node = engine_root.children[0].children[0]
    # The methods shouldn't be fetched by name with the default parse_tree call
    assert third_node.exposed_handlers[engine.DataTarget.RAW] != "RAW handler response"

    #Add a non ChordataBaseNode derivate node
    r_node = chordata_nodetree.nodes.new(fake_regular_node)
    socket_in = r_node.inputs[0]
    socket_out = n_node.outputs[0]
    chordata_nodetree.links.new(socket_in, socket_out)

    #The EngineNode constructor should raise a TypeError if the node is not a
    #ChordataBaseNode derivate
    with pytest.raises(TypeError):
        engine.EngineNode(r_node)

    #The alien node should not be included in the hierarchy
    node_root = engine_instance.parse_tree(n_node)
    assert len(node_root.children) == 1
    assert node_root.children[0].from_node == t_node.name

def test_enginenode_alone(engine_node, copp_Q_packet):
    # test for empty children
    assert engine_node.children == []

    # test for empty data call
    with pytest.raises(TypeError):
        engine_node()

    # test for proper call with no handlers (no children connected)
    assert engine_node(copp_Q_packet) == None


def test_enginode_connections(test_node_tree):
    node_1 = test_node_tree["nodes"][0]
    node_2 = test_node_tree["nodes"][1]
    node_3 = test_node_tree["nodes"][2]
    
    # these should fail for avoiding loops
    node_2.connect(node_1)
    node_3.connect(node_2)
    node_3.connect(node_1)
    # test illegal connections being avoided
    assert node_1.children == [node_2]
    assert node_2.children == [node_3]
    assert node_2.parent == node_1
    assert node_3.parent == node_2
    assert node_3.children == []


def test_enginenode(test_node_tree, engine, copp_Q_packet):
    node_1 = test_node_tree["nodes"][0]
    counter = test_node_tree["counter"]

    # test correct number of calls to handlers
    node_1(copp_Q_packet)
    assert counter['Dump Node.001']['Q_handler'] == 1

    copp_Q_packet.target = engine.DataTarget.ERR
    test_string = "TEST ERROR MESSAGE"
    copp_Q_packet.payload = test_string
    node_1(copp_Q_packet)
    assert counter['Dump Node.001']['Q_handler'] == 1
    assert counter['Dump Node.001']['ERR_handler'] == 1
    assert counter['Dump Node.002']['ERR_handler'] == 1
    assert counter['Dump Node.003']['ERR_handler'] == 1
    assert copp_Q_packet.payload == test_string[:-3]

def test_enginenode_settings(base_fake_nodetree, engine):
    t_node, n_node, f_node = base_fake_nodetree
    engine_instance = engine.Engine()
    engine_root = engine_instance.parse_tree(n_node)

    #the `settings` property should not be used within an EngineNode
    with pytest.raises(AttributeError):
        engine_root.settings.local_address

    assert type(engine_root.id_settings.local_address) == str


# def test_enginenode_add_subtarget(vector_testcube_nodetree, engine, copp_Q_packet, copp_RAW_packet):
#     t_node, n_node, v_node = vector_testcube_nodetree
#     engine_instance = engine.Engine()
#     engine_root = engine_instance.parse_tree(n_node)

#     #the first time just one of the nodes should contrbute to the 
#     #id_tree.subtargets list
#     engine_root(copp_Q_packet)
#     engine_root(copp_RAW_packet)

#     copp_Q_packet.restore()
#     copp_RAW_packet.restore()

#     q_subtargets = True
#     for msg in copp_Q_packet.get():
#         if msg.subtarget not in engine_root.id_tree.subtargets:
#             q_subtargets = False
#             break

#     raw_subtargets = True
#     for msg in copp_RAW_packet.get():
#         if msg.subtarget not in engine_root.id_tree.subtargets:
#             raw_subtargets = False
#             break

#     #just one of them add its subtargets
#     assert not (raw_subtargets and q_subtargets)

#     copp_Q_packet.restore()
#     copp_RAW_packet.restore()

#     #the second time just the other node should contrbute to the 
#     #id_tree.subtargets list
#     engine_root(copp_Q_packet)
#     engine_root(copp_RAW_packet)

#     copp_Q_packet.restore()
#     copp_RAW_packet.restore()

#     q_subtargets = True
#     for msg in copp_Q_packet.get():
#         if msg.subtarget not in engine_root.id_tree.subtargets:
#             q_subtargets = False
#             break

#     raw_subtargets = True
#     for msg in copp_RAW_packet.get():
#         if msg.subtarget not in engine_root.id_tree.subtargets:
#             raw_subtargets = False
#             break

#     #test both of them add its subtargets
#     assert raw_subtargets and q_subtargets