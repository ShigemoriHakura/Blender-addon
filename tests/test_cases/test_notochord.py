import pytest
import functools


@pytest.fixture
def notochord_ops(chordata_module):
    return chordata_module.ops.notochord_ops


@pytest.fixture
def engine(chordata_module):
    return chordata_module.ops.engine


def test_call(chordata_nodetree, engine, copp_Q_packet):
    notochord_node = chordata_nodetree.nodes.new('NotochordNodeType')
    debug_node = chordata_nodetree.nodes.new('DumpNodeType')

    nodetree_engine = engine.Engine()

    # connect notochord to armature
    chordata_nodetree.links.new(
        debug_node.inputs['debug_in'], notochord_node.outputs['capture_out'])

    # parse engine tree and return root
    engine_root = nodetree_engine.parse_tree(notochord_node)

    # test notochord engine node call
    assert engine_root(copp_Q_packet) == copp_Q_packet

@pytest.fixture 
def base_notochord_tree(chordata_nodetree, chordata_avatar):
    notochord_node = chordata_nodetree.nodes.new('NotochordNodeType')
    debug_node1 = chordata_nodetree.nodes.new('DumpNodeType')
    armature_node = chordata_nodetree.nodes.new('ArmatureNodeType')
    armature_node.settings.armature_ob = chordata_avatar
    debug_node2 = chordata_nodetree.nodes.new('DumpNodeType')


    # connect notochord to armature
    chordata_nodetree.links.new(
        armature_node.inputs['capture_in'], notochord_node.outputs['capture_out'])
    # connect notochord to debug1
    chordata_nodetree.links.new(
        debug_node1.inputs['debug_in'], notochord_node.outputs['capture_out'])
    # connect armature to debug2
    chordata_nodetree.links.new(
        debug_node2.inputs['debug_in'], armature_node.outputs['data_out'])

    return notochord_node, debug_node1, armature_node, debug_node2


@pytest.fixture
def notochord_engine_root(chordata_nodetree, base_notochord_tree, TestNodeClass, engine):
    notochord_node, debug_node1, armature_node, debug_node = base_notochord_tree
    TestNode, counter = TestNodeClass
    nodetree_engine = engine.Engine()

    # parse engine tree and return root
    engine_root = nodetree_engine.parse_tree(notochord_node)

    return {"node": notochord_node, "eng_node":engine_root, "nodetree":chordata_nodetree}

@pytest.fixture
def notochord_engine_root_TestNode(chordata_nodetree, base_notochord_tree, TestNodeClass, engine):
    notochord_node, debug_node1, armature_node, debug_node = base_notochord_tree
    TestNode, counter = TestNodeClass
    nodetree_engine = engine.Engine(NodeClass = TestNode)

    # parse engine tree and return root
    engine_root = nodetree_engine.parse_tree(notochord_node)

    return {"node": notochord_node, "eng_node":engine_root, "nodetree":chordata_nodetree}


def test_engine_parse_tree(notochord_engine_root, base_notochord_tree, engine):
    notochord_node, debug_node1, armature_node, debug_node = base_notochord_tree
    engine_root = notochord_engine_root["eng_node"]
   
    # test connections
    assert len(engine_root.children) == 2
    assert len(engine_root.children[0].children) == 1
   
    # test functions were fetched
    assert engine_root.exposed_handlers[engine.DataTarget.Q].func == type(
        notochord_node).q_handler
    assert engine_root.children[0].exposed_handlers[engine.DataTarget.Q].func == type(armature_node).process_quats
    assert engine_root.children[0].exposed_handlers[engine.DataTarget.Q] != type(
        notochord_node).q_handler


def test_demo_mode(notochord_engine_root, bpy_test):
    chordata_nodetree = notochord_engine_root["nodetree"]
    notochord_node = notochord_engine_root["node"]

    # start demo engine mode
    bpy_test.ops.chordata.notochord_demo(
        from_tree=chordata_nodetree.name, from_node=notochord_node.name)

    # test flags were set
    assert chordata_nodetree.engine_running == True
    assert chordata_nodetree.engine_stop == False

    # removes notochord node
    chordata_nodetree.nodes.remove(notochord_node)

    # test notochord free function was called on removal and flag was set
    assert chordata_nodetree.engine_stop == True


def test_free_onenginenotrunning(chordata_nodetree, bpy_test, engine, base_notochord_tree):
    notochord_node, debug_node1, armature_node, debug_node = base_notochord_tree

    nodetree_engine = engine.Engine()
    # parse engine tree and return root
    engine_root = nodetree_engine.parse_tree(notochord_node)

    # test flags were not set
    assert chordata_nodetree.engine_running == False
    assert chordata_nodetree.engine_stop == False

    # removes notochord node
    chordata_nodetree.nodes.remove(notochord_node)

    # test nothing has changed
    assert chordata_nodetree.engine_running == False
    assert chordata_nodetree.engine_stop == False
