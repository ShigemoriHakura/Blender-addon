import pytest

import bpy
import addon_utils

import sys

extra_args = []
try:
    separator_pos = sys.argv.index("--")
    extra_args = sys.argv[ separator_pos+1 : ]
    print("== Running pytest with args:", extra_args ,"==")
except ValueError:
    pass


from os.path import join, abspath, dirname
dev_script_folder = join(dirname(abspath(__file__)), "..")

addon = "chordata"


class SetupPlugin(object):
    def __init__(self, addon):
        self.addon = addon

    def pytest_configure(self, config):
        print("Disabling all add-ons")
        addon_utils.disable_all()
        bpy.context.preferences.filepaths.script_directory = dev_script_folder
        bpy.utils.refresh_script_paths()
        
        print("Enabling add-on", addon)
        bpy.ops.preferences.addon_enable(module=addon)
        config.cache.set("chordata_addon", self.addon)

    def pytest_unconfigure(self):
        bpy.ops.preferences.addon_disable(module=self.addon)
        print("*** Test run reporting finished, cleanup done ***")


try:
    exit_val = pytest.main(["test_cases"] + extra_args, plugins=[SetupPlugin(addon)])
except Exception as e:
    print(e)
    exit_val = 1
sys.exit(exit_val)

